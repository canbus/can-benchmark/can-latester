# CAN Bus Latency Tester (Benchmarking Utilities)

This project provides actual tools to measure CAN frames
round-trip latencies through CAN gateway Device Under
the Test (DUT) forwarding CAN frames from one CAN bus
interface to another one.

The CAN frame are sent by device which needs to provide
precise timestamping for monitored messages on the
sending and receiving CAN buses.

The `libtalloc-dev` and `libpopt-dev` packages are required
to build the tools same as Linux kernel SocketCAN user space
API headers.

The actual Latency Tester project subgroup is located at

[https://gitlab.fel.cvut.cz/canbus/can-benchmark](https://gitlab.fel.cvut.cz/canbus/can-benchmark)

it hosts this tools projects as well as tools to run continuous
Linux kernel CAN subsystem latencies benchmarking

- [CAN Latency Tester Tool](https://gitlab.fel.cvut.cz/canbus/can-benchmark/can-latester)
- [CAN Latester Automation](https://gitlab.fel.cvut.cz/canbus/can-benchmark/can-latester-automation)

the actual output is located at

- [https://canbus.pages.fel.cvut.cz/can-latester/](https://canbus.pages.fel.cvut.cz/can-latester/)

The [CTU CAN FD IP core](https://gitlab.fel.cvut.cz/canbus/ctucanfd_ip_core)
on Zynq based [MZ_APO boards](https://cw.fel.cvut.cz/wiki/courses/b35apo/en/documentation/mz_apo/start)
is used for precise CAN/CAN FD frames timestamping on this reference setup
but other SocketCAN drivers supporting hardware timestamping of received CAN
frames can be used as well.

The CAN projects and activities at [Czech Technical University in Prague (CTU)](https://www.cvut.cz/) [FEE](https://fel.cvut.cz/en/) guidepost main address is

- [https://canbus.pages.fel.cvut.cz/](https://canbus.pages.fel.cvut.cz/)
