#!/usr/bin/env python3
from argparse import ArgumentParser
from itertools import product
from os import system, path
from typing import Tuple, Iterable
from collections import namedtuple
from time import sleep
from datetime import datetime

Combination = namedtuple("Combination", ["flood_messages", "messages_count", "frame_length"])

parser = ArgumentParser(description="Automate multiple latester measurements.")
# parser.add_argument("--driver-name", default="timestampy")
parser.add_argument("--with-load", action="store_true")
parser.add_argument("--cangen-spam", action="store_true")
parser.add_argument("--bus-speed", required=True, choices=[125000, 500000, 1000000], type=int)
parser.add_argument("--additional-comment", type=str)
args = parser.parse_args()

def global_setup(bus_speed: int):
    # system(f"/devel/latester/global_setup.sh timestampy {args.bus_speed}")
    if args.cangen_spam:
        print("NOW IS YOUR TIME TO START SPAMMING")
        sleep(10)
        print("YOUR TIME IS OVER")
    assert(path.exists("/dev/uio0"))

def run_latester(comb: Combination):
    identifier = f"automated-speed{args.bus_speed}-flood{comb.flood_messages}-frameLength{comb.frame_length}-messagesCount{comb.messages_count}-load{args.with_load}{('-' + args.additional_comment) if args.additional_comment else ''}-{datetime.now().isoformat()}"
    system(f"./latester -d can2 -d can3 -d can4 -n {identifier} -l {comb.frame_length} -b {args.bus_speed} -c {comb.messages_count} {'' if comb.flood_messages else '-o'} > log-{identifier}.txt 2>&1")

def run_tests(combinations: Iterable[Tuple[int, bool, int]]):
    global_setup(args.bus_speed)
    for f in combinations:
        comb = Combination(*f)
        run_latester(comb)
        print(f"Run with parameters {f}")
        sleep(1)


def main():
    flood_messages = [True, False]
    #messages_count = [100, 1000, 10000]
    messages_count = [3200]
    frame_lengths = [2, 4, 8]
    testing_combinations = product(flood_messages, messages_count, frame_lengths)
    run_tests(testing_combinations)

if __name__ == "__main__":
    main()