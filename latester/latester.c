/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * CAN Latency Tester
 * Copyright (C) 2010, 2011, 2012, 2013, 2014 Michal Sojka, DCE, FEE, CTU Prague
 * Copyright (C) 2016 Martin Jerabek, DCE, FEE, CTU Prague
 * Copyright (C) 2022 Matej Vasilevski, DCE, FEE, CTU Prague
 * Copyright (C) 2023 Pavel Hronek, DCE, FEE, CTU Prague
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/*
 * The project is continuation to DCE, FEE, CTU Prague OCERA OrtCAN
 * project (origin 2003) CANping and other drivers and tools
 * which has been started and is maintained by Pavel Pisa at CTU FEE.
 * See CTU FEE CAN bus projects guidepost for components, continuous
 * integration, live results etc. https://canbus.pages.fel.cvut.cz/
 */

#include <time.h>
#define _GNU_SOURCE
#include <ctype.h>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <math.h>
#include <net/if.h>
#include <poll.h>
#include <popt.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <talloc.h>
#include <unistd.h>

#include "canframelen.h"
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>

#include "histogram.h"

//#define FTRACE

#define NSEC_PER_SEC 1000000000L

#ifndef DEBUG
#define dbg(level, fmt, arg...) do {} while (0)
#else
#define dbg(level, fmt, arg...) do { if (level <= DEBUG) { printf("candping: " fmt, ## arg); } } while (0)
#endif

#define INTERRUPTED_SYSCALL(errno) (errno == EINTR || errno == ERESTART)

#define MEMSET_ZERO(obj) memset(&(obj), 0, sizeof(obj))

/* Global variables */
volatile sig_atomic_t finish_flag = 0;	/* Threads should terminate. */
sem_t finish_sem;		/* Thread signals a termination */

/* Command line options */
struct options {
	char **interface;
	canid_t id;
	unsigned period_us;
	unsigned timeout_ms;
	unsigned count;
	unsigned oneattime;
	unsigned bitrate;
	unsigned dbitrate;
	char *name;
	int length;
	int userhist;
	int quiet;
	int fd;
	int one_bus_meas;
	int disable_can_filters;

	/* Temporary variables */
	FILE *f_msgs;
	FILE *f_hist;
	FILE *f_hist_gw;
	FILE *f_stat;
};

#define SYNC_MESSAGE_ID 0

struct options opt = {
	.id = 10,
	.period_us = 0,
	.timeout_ms = 1000,
	.length = 2,
	.bitrate = 1000000,
	.dbitrate = 2000000
};

struct {
	unsigned enobufs;
	unsigned overrun;
	unsigned lost;
	struct timespec tic, tac;
	unsigned timeouts;
	unsigned invalid_frame;
} stats;

int num_interfaces = 0;
int count = 0;			/* Number of sent messages */
unsigned msg_in_progress = 0;
int completion_pipe[2];
struct timespec can_tsdiff = {0, 0}; /* can_rx2_ts - can_rx1_ts */

struct msg_info {
	canid_t id;
	uint8_t length;
	struct timespec ts_sent, ts_sent_kern;
	struct timespec ts_rx_onwire, ts_rx_onwire_kern;
	struct timespec ts_rx_final, ts_rx_final_kern;
	struct canfd_frame sent, received;
};

#define MAX_INFOS 10000
struct msg_info msg_infos[MAX_INFOS];

struct histogram histogram, histogram_gw;

void sprint_canframe(char *buf , struct canfd_frame *cf, int sep) {
	/* documentation see lib.h */

	int i,offset;
	int dlc = cf->len;

	if (cf->can_id & CAN_ERR_FLAG) {
		sprintf(buf, "%08X#", cf->can_id & (CAN_ERR_MASK|CAN_ERR_FLAG));
		offset = 9;
	} else if (cf->can_id & CAN_EFF_FLAG) {
		sprintf(buf, "%08X#", cf->can_id & CAN_EFF_MASK);
		offset = 9;
	} else {
		sprintf(buf, "%03X#", cf->can_id & CAN_SFF_MASK);
		offset = 4;
	}

	if (cf->can_id & CAN_RTR_FLAG) /* there are no ERR frames with RTR */
		sprintf(buf+offset, "R");
	else
		for (i = 0; i < dlc; i++) {
			sprintf(buf+offset, "%02X", cf->data[i]);
			offset += 2;
			if (sep && (i+1 < dlc))
				sprintf(buf+offset++, ".");
		}
}

static inline uint16_t frame_index(struct canfd_frame *frame)
{
	uint16_t idx;
	if (frame->len >= 2) {
		memcpy(&idx, frame->data, sizeof(idx));
		if (idx >= MAX_INFOS)
			error(1, 0, "%s idx too high: %u", __FUNCTION__, idx);
	} else {

		error(1, 0, "%s error", __FUNCTION__);
	}
	return idx;
}

static inline struct msg_info *frame2info(struct canfd_frame *frame)
{
	return &msg_infos[frame_index(frame)];
}

static inline char *tstamp_str(const void *ctx, struct timespec *tstamp)
{
	return talloc_asprintf(ctx, "%ld.%06ld",
			       tstamp->tv_sec, tstamp->tv_nsec/1000);
}

static inline size_t get_can_bus_mtu()
{
	return opt.fd ? CANFD_MTU : CAN_MTU;
}

int timespec_subtract (struct timespec *result, const struct timespec *x, const struct timespec *yy);

static inline unsigned get_timestamp_diff_us(struct msg_info *mi);

void msg_info_process(FILE *f, struct msg_info *mi)
{
	struct timespec diff;
	void *local = talloc_new (NULL);
	static long num = 0;
	char sent[256], received[256];
	int diff_us = get_timestamp_diff_us(mi);
	int tx_time_us = 0;

	if (opt.fd){
		tx_time_us = (int)round(
			(1e6/opt.bitrate)*can_frame_length(&mi->received, CFL_WORSTCASE, CANFD_MTU)
			+ (1e6/opt.dbitrate)*can_frame_dbitrate_length(&mi->received, CFL_WORSTCASE, CANFD_MTU)
		);
	}
	else {
		tx_time_us = (int)round(
			(1e6/opt.bitrate)*can_frame_length(&mi->received, CFL_EXACT, CAN_MTU)
		);
	}

	/* Update histograms */
	histogram_add(&histogram, diff_us);
	histogram_add(&histogram_gw, diff_us - tx_time_us);

	if (!f)
		return;

	/* Print information to a file */
	sprint_canframe(sent, &mi->sent, true);
	sprint_canframe(received, &mi->received, true);

#define S(ts) tstamp_str(local, &ts)
#define DIFF(a, b) (timespec_subtract(&diff, &b, &a), S(diff))

	switch (num_interfaces) {
	case 2:
		fprintf(f, "%ld: %s %s -> %s (%s) %s = %s (%s) %d\n",
			num, S(mi->ts_sent), sent, S(mi->ts_rx_final_kern), S(mi->ts_rx_final), received,
			DIFF(mi->ts_sent, mi->ts_rx_final_kern),
			DIFF(mi->ts_sent, mi->ts_rx_final),
			tx_time_us);
		break;
	case 3:
		fprintf(f, "%ld: %s %s -> %s (%s) -> %s (%s) %s = %s (%s), %s (%s) %d %.6f\n",
			num, S(mi->ts_sent), sent,
			S(mi->ts_rx_onwire_kern), S(mi->ts_rx_onwire),
			S(mi->ts_rx_final_kern), S(mi->ts_rx_final), received,
			DIFF(mi->ts_sent, mi->ts_rx_onwire_kern),
			DIFF(mi->ts_sent, mi->ts_rx_onwire),
			DIFF(mi->ts_rx_onwire_kern, mi->ts_rx_final_kern),
			DIFF(mi->ts_rx_onwire, mi->ts_rx_final),
			tx_time_us,
			1e-6*(diff_us - tx_time_us));
		break;
	}
#undef S
#undef DIFF
	num++;
	talloc_free (local);
}

/* Subtract the `struct timespec' values X and Y, storing the result in
   RESULT.  Return 1 if the difference is negative, otherwise 0.  */

int timespec_subtract (struct timespec *result, const struct timespec *x, const struct timespec *yy)
{
	struct timespec ylocal = *yy, *y = &ylocal;
	/* Perform the carry for the later subtraction by updating Y. */
	if (x->tv_nsec < y->tv_nsec) {
		int nsec = (y->tv_nsec - x->tv_nsec) / 1000000000 + 1;
		y->tv_nsec -= 1000000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_nsec - y->tv_nsec > 1000000000) {
		int nsec = (x->tv_nsec - y->tv_nsec) / 1000000000;
		y->tv_nsec += 1000000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait.
	   `tv_nsec' is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_nsec = x->tv_nsec - y->tv_nsec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}

void dbg_print_timespec(char *msg, struct timespec *tv)
{

	printf("%s sec=%ld nsec=%ld\n", msg, tv->tv_sec, tv->tv_nsec);
}

static inline unsigned get_timestamp_diff_us(struct msg_info *mi)
{
	struct timespec diff;
	struct timespec diff_tmp;
	switch (num_interfaces) {
	case 3:
		if (opt.userhist)
			timespec_subtract(&diff, &mi->ts_rx_final, &mi->ts_rx_onwire);
		else {
			timespec_subtract(&diff, &mi->ts_rx_final_kern, &mi->ts_rx_onwire_kern);
			timespec_subtract(&diff_tmp, &diff, &can_tsdiff);
			diff = diff_tmp;
		}
		break;
	case 2:
		if (opt.userhist)
			timespec_subtract(&diff, &mi->ts_rx_final, &mi->ts_sent);
		else
			timespec_subtract(&diff, &mi->ts_rx_final_kern, &mi->ts_sent);
		break;
	default:
		return 0;
	}
	unsigned us = diff.tv_sec * 1000000 + (diff.tv_nsec + 500)/1000;
	return us;
}

void set_sched_policy_and_prio(int policy, int rtprio)
{
	struct sched_param scheduling_parameters;
	int maxprio=sched_get_priority_max(policy);
	int minprio=sched_get_priority_min(policy);

	if((rtprio < minprio) || (rtprio > maxprio))
		error(1, 0, "The priority for requested policy is out of <%d, %d> range\n",
		      minprio, maxprio);

	scheduling_parameters.sched_priority = rtprio;

	if (0 != pthread_setschedparam(pthread_self(), policy, &scheduling_parameters))
		error(1, errno, "pthread_setschedparam error");
}

void term_handler(int signum)
{
	finish_flag = 1;
}

static inline int sock_get_if_index(int s, const char *if_name)
{
	struct ifreq ifr;
	MEMSET_ZERO(ifr);

	strcpy(ifr.ifr_name, if_name);
	if (ioctl(s, SIOCGIFINDEX, &ifr) < 0)
		error(1, errno, "SIOCGIFINDEX '%s'", if_name);
	return ifr.ifr_ifindex;
}

static inline void get_tstamp(struct timespec *ts)
{
	clock_gettime(CLOCK_REALTIME/*MONOTONIC*/, ts);
}


int trace_fd = -1;
int marker_fd = -1;

void init_ftrace()
{
#ifdef FTRACE
	char *debugfs;
	char path[256];
	FILE *f;

	debugfs = "/sys/kernel/debug";
	if (debugfs) {
		strcpy(path, debugfs);
		strcat(path,"/tracing/tracing_on");
		trace_fd = open(path, O_WRONLY);
		if (trace_fd >= 0)
			write(trace_fd, "1", 1);

		strcpy(path, debugfs);
		strcat(path,"/tracing/trace_marker");
		marker_fd = open(path, O_WRONLY);

		strcpy(path, debugfs);
		strcat(path,"/tracing/set_ftrace_pid");
		f = fopen(path, "w");
		fprintf(f, "%d\n", getpid());
		fclose(f);
		system("echo function_graph > /sys/kernel/debug/tracing/current_tracer");
		system("echo can_send > /sys/kernel/debug/tracing/set_graph_function");
		system("echo > /sys/kernel/debug/tracing/trace");
		system("echo 1 > /sys/kernel/debug/tracing/tracing_enabled");
	}
#endif	/* FTRACE */
}

static inline void trace_on()
{
	if (trace_fd >= 0)
		write(trace_fd, "1", 1);
}

static inline void trace_off(int ret)
{
	if (marker_fd >= 0) {
		char marker[100];
		sprintf(marker, "write returned %d\n", ret);
		write(marker_fd, marker, strlen(marker));
	}
	if (trace_fd >= 0)
		write(trace_fd, "0", 1);
}

static inline void msg_info_free(struct msg_info *mi)
{
	mi->id = -1;
}

static inline bool msg_info_used(struct msg_info *mi)
{
	return mi->id != -1;
}

int send_frame(int socket)
{
	struct canfd_frame frame;
	struct msg_info *mi;
	int ret;
	static int curr_msg = -1;
	int i;
	uint16_t idx;

	MEMSET_ZERO(frame);
	i = curr_msg+1;
	while (msg_info_used(&msg_infos[i]) && i != curr_msg) {
		i++;
		if (i >= MAX_INFOS)
			i = 0;
	}
	if (i == curr_msg)
		error(1, 0, "Msg info table is full! Probably, many packets were lost.");
	else
		curr_msg = i;

	frame.can_id = opt.id;
	if (opt.length < 2)
		error(1, 0, "Length < 2 is not yet supported");
	frame.len = opt.length;
	idx = curr_msg;
	memcpy(frame.data, &idx, sizeof(idx));
	mi = frame2info(&frame);

	mi->id = frame.can_id;
	mi->length = frame.len;
	get_tstamp(&mi->ts_sent);
	mi->sent = frame;

	trace_on();
	ret = write(socket, &frame, get_can_bus_mtu());
	trace_off(ret);

	if (ret == -1 || num_interfaces == 1)
		msg_info_free(mi);
	return ret;
}

static inline void send_and_check(int s)
{
	int ret;
	ret = send_frame(s);
	if (ret != get_can_bus_mtu()) {
/* 		if (ret == -1 && errno == ENOBUFS && opt.period_us == 0 && !opt.oneattime) { */
/* 			stats.enobufs++; */
/* 			/\* Ignore this error - pfifo_fast qeuue is full *\/ */
/* 		} else */
			error(1, errno, "send_frame (line %d)", __LINE__);
	} else {
		count++;
		msg_in_progress++;
	}
}

static inline void get_next_timeout(struct timespec *timeout, bool advance)
{
	struct timespec now;
	static struct timespec last = {-1, 0 };

	clock_gettime(CLOCK_MONOTONIC, &now);

	if (last.tv_sec == -1) {
		last = now;
		last.tv_nsec = last.tv_nsec/1000000*1000000;
	}
	if (opt.period_us != 0) {
		if (advance) {
			last.tv_sec += opt.period_us/1000000;
			last.tv_nsec += (opt.period_us%1000000)*1000;
			while (last.tv_nsec >= 1000000000) {
				last.tv_nsec -= 1000000000;
				last.tv_sec++;
			}
		}
		if (timespec_subtract(timeout, &last, &now) /* is negative */) {
			stats.overrun++;
			memset(timeout, 0, sizeof(*timeout));
		}
/* 		printf("next %ld.%06ld  now %ld.%06ld  --> timeout %ld.%06ld\n", */
/* 		       last.tv_sec, last.tv_nsec/1000, */
/* 		       now.tv_sec, now.tv_nsec/1000, */
/* 		       timeout->tv_sec, timeout->tv_nsec/1000); */
	} else if (opt.timeout_ms != 0) {
		timeout->tv_sec = opt.timeout_ms/1000;
		timeout->tv_nsec = (opt.timeout_ms%1000)*1000000;
	} else
		error(1, 0, "Timeout and period cannot be both zero");
}

void receive(int s, struct canfd_frame *frame, struct timespec *ts_kern, struct timespec *ts_user)
{
	char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	struct sockaddr_can addr;
	int nbytes;
	static uint64_t dropcnt = 0;
	bool have_hwtstamp = false;

	iov.iov_base = frame;
	msg.msg_name = &addr;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = &ctrlmsg;

	/* these settings may be modified by recvmsg() */
	iov.iov_len = sizeof(*frame);
	msg.msg_namelen = sizeof(addr);
	msg.msg_controllen = sizeof(ctrlmsg);
	msg.msg_flags = 0;

	nbytes = recvmsg(s, &msg, 0);
	if (nbytes < 0)
		error(1, errno, "recvmsg");

	if (nbytes < get_can_bus_mtu())
		error(1, 0, "recvmsg: incomplete CAN frame\n");

	bool timestamp_set = false;
	get_tstamp(ts_user);
	MEMSET_ZERO(*ts_kern);
	for (cmsg = CMSG_FIRSTHDR(&msg);
	     cmsg && (cmsg->cmsg_level == SOL_SOCKET);
	     cmsg = CMSG_NXTHDR(&msg,cmsg)) {
		if (cmsg->cmsg_type == SCM_TIMESTAMPNS) {
			if(!have_hwtstamp) {
				memcpy(ts_kern, CMSG_DATA(cmsg), sizeof(struct timespec));
				timestamp_set = true;
			}
		}
		else if (cmsg->cmsg_type == SCM_TIMESTAMPING) {
			struct scm_timestamping *scm_ts = (struct scm_timestamping *) CMSG_DATA(cmsg);
			memcpy(ts_kern, &scm_ts->ts[2], sizeof(struct timespec));
			have_hwtstamp = true;
			timestamp_set = true;
		}
		else if (cmsg->cmsg_type == SO_RXQ_OVFL) {
			uint32_t ovfl;
			memcpy(&ovfl, CMSG_DATA(cmsg), sizeof(ovfl));
			dropcnt += ovfl;
		}
	}

}

void process_tx(int s)
{
	error(1, 0, "%s: not implemented", __FUNCTION__);
}

void process_on_wire_rx(int s)
{
	struct timespec ts_kern, ts_user;
	struct canfd_frame frame;
	struct msg_info *mi;

	receive(s, &frame, &ts_kern, &ts_user);
	if (opt.one_bus_meas && (frame.can_id != opt.id)) {
		return;
	}

	mi = frame2info(&frame);
	if (msg_info_used(mi)) {
		mi->ts_rx_onwire_kern = ts_kern;
		mi->ts_rx_onwire = ts_user;
	} else
		stats.invalid_frame++;
}


int process_final_rx(int s)
{
	struct timespec ts_kern, ts_user;
	struct canfd_frame frame;
	struct msg_info *mi;
	int ret;

	receive(s, &frame, &ts_kern, &ts_user);
	if (opt.one_bus_meas && (frame.can_id != (opt.id - 1))) {
		return -1;
	}

	mi = frame2info(&frame);
	mi->ts_rx_final_kern = ts_kern;
	mi->ts_rx_final = ts_user;
	mi->received = frame;

	ret = write(completion_pipe[1], &mi, sizeof(mi));
	if (ret == -1)
		error(1, errno, "completion_pipe write");

	return 0;
}

int determine_can_timing_offset(struct pollfd *pfd, int nifc)
{
	int uiofd, retval = 0;
	volatile uint32_t *can_crossbar;
	uint32_t oldreg;
	struct canfd_frame frame;
	struct timespec ts_rx1, ts_rx2, ts_dummy;
	struct timespec one_sec_timeout = {.tv_sec = 1, .tv_nsec = 0};

	int res;

	if(nifc < 2)
		return -1;

	uiofd = open("/dev/uio0", O_RDWR);
	if(uiofd < 0) {
		perror("determine_can_timing_offset: /dev/uio0");
		return -1;
	}

	can_crossbar = (volatile uint32_t *) mmap(NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, uiofd, 0);
	if(can_crossbar == MAP_FAILED) {
		perror("mmap");
		retval = -1;
		goto cleanup1;
	}

	oldreg = can_crossbar[0];
	can_crossbar[0] = 0x01000000; // connect all CAN interfaces together on Line1, and enable Line1's external output
		// for more details, see page 45 in
		// https://dspace.cvut.cz/bitstream/handle/10467/80366/F3-DP-2019-Jerabek-Martin-Jerabek-thesis-2019-canfd.pdf

	MEMSET_ZERO(frame);
	frame.can_id = SYNC_MESSAGE_ID;
	frame.len = 4;
	memcpy(frame.data, "SYNC", 4);
	res = write(pfd[0].fd, &frame, get_can_bus_mtu());
	if(res != get_can_bus_mtu()) {
		perror("failed to write SYNC message");
		retval = -1;
		goto cleanup2;
	}

	if (ppoll(&pfd[1], 1, NULL, NULL) < 0) {
		perror("failed to receive SYNC message from rx1");
		retval = -1;
		goto cleanup2;
	}
	if (pfd[1].revents & POLLIN) {
		receive(pfd[1].fd, &frame, &ts_rx1, &ts_dummy);
		pfd[1].revents = 0;
		if (strncmp(frame.data, "SYNC", 4) != 0) {
			fprintf(stderr, "Didn't receive SYNC message on rx1, instead got %.5s\n", frame.data);
			retval = -1;
			goto cleanup2;
		}
		if (frame.can_id != SYNC_MESSAGE_ID) {
			fprintf(stderr, "Didn't receive SYNC message id on rx1, instead got %d\n", frame.can_id);
			retval = -1;
			goto cleanup2;
		}
	} else {
		fprintf(stderr, "Unexpected pfd[1].revents when polling SYNC message: 0x%04x\n", pfd[1].revents);
	}

	if (ppoll(&pfd[2], 1, NULL, NULL) < 0) {
		perror("failed to receive SYNC message from rx2");
		retval = -1;
		goto cleanup2;
	}
	if (pfd[2].revents & POLLIN) {
		receive(pfd[2].fd, &frame, &ts_rx2, &ts_dummy);
		pfd[2].revents = 0;
		if (strncmp(frame.data, "SYNC", 4) != 0) {
			fprintf(stderr, "Didn't receive SYNC message data on rx2, instead got %.5s\n", frame.data);
			retval = -1;
			goto cleanup2;
		}
		if (frame.can_id != SYNC_MESSAGE_ID) {
			fprintf(stderr, "Didn't receive SYNC message id on rx2, instead got %d\n", frame.can_id);
			retval = -1;
			goto cleanup2;
		}
	} else {
	 	fprintf(stderr, "Unexpected pfd[2].revents when polling SYNC message: 0x%04x\n", pfd[2].revents);
	}

	timespec_subtract(&can_tsdiff, &ts_rx2, &ts_rx1);

cleanup2:
	can_crossbar[0] = oldreg;
	munmap((void*)can_crossbar, 4096);
cleanup1:
	close(uiofd);

	// After reverting the crossbar config, the SYNC message has to be received once more
	if (ppoll(&pfd[2], 1, &one_sec_timeout, NULL) > 0) {
		if (pfd[2].revents & POLLIN) {
			receive(pfd[2].fd, &frame, &ts_rx2, &ts_dummy);
			pfd[2].revents = 0;
		}
	}

	if (ppoll(&pfd[1], 1, &one_sec_timeout, NULL) > 0) {
		if (pfd[1].revents & POLLIN) {
			receive(pfd[1].fd, &frame, &ts_rx2, &ts_dummy);
			pfd[1].revents = 0;
		}
	}

	return retval;
}

void *measure_thread(void *arg)
{
	int s, i, ret;
	struct pollfd pfd[3];
	struct timespec timeout;
	struct sockaddr_can addr;
	int consecutive_timeouts = 0;

	MEMSET_ZERO(pfd);

	for (i=0; i<num_interfaces; i++) {
		if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
			error(1, errno, "socket");
		int enable_canfd = 1;
		if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &enable_canfd, sizeof(enable_canfd)) == -1){
			int errnum = errno;
			printf("SOL_CAN_RAWv2 errno %d, message: %s\n", errnum, strerror(errnum));
			error(1, errno, "SOL_CAN_RAWv2");
		}
		addr.can_family = AF_CAN;
		addr.can_ifindex = sock_get_if_index(s, opt.interface[i]);

		if (i == 0) { 	/* TX socket */
			/* disable default receive filter on this RAW socket */
			/* This is obsolete as we do not read from the socket at all, but for */
			/* this reason we can remove the receive list in the Kernel to save a */
			/* little (really a very little!) CPU usage.                          */
			if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0) == -1){
				int errnum = errno;
				printf("SOL_CAN_RAW errno %d, message: %s\n", errnum, strerror(errnum));
				error(1, errno, "SOL_CAN_RAW");
			}
		}

		if (i == 1) { /* RX1 socket */
			if (opt.one_bus_meas && !opt.disable_can_filters) {
				struct can_filter rfilter;
				rfilter.can_id   = (opt.id - 1) | CAN_INV_FILTER;
				rfilter.can_mask = CAN_SFF_MASK;
				if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter))){
					int errnum = errno;
					printf("RX1 CAN filter errno %d, message: %s\n", errnum, strerror(errnum));
					error(1, errno, "RX1 CAN filter");
				}
			}
		}

		if (i == 2) { /* RX2 socket */
			if (opt.one_bus_meas && !opt.disable_can_filters) {
				struct can_filter rfilter;

				rfilter.can_id   = opt.id | CAN_INV_FILTER;
				rfilter.can_mask = CAN_SFF_MASK;
				if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter))){
					int errnum = errno;
					printf("RX2 CAN filter errno %d, message: %s\n", errnum, strerror(errnum));
					error(1, errno, "RX2 CAN filter");
				}
			}
		}

		if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
			error(1, errno, "bind");

		const int so_timestamping_flags = SOF_TIMESTAMPING_RX_HARDWARE | SOF_TIMESTAMPING_RAW_HARDWARE;
		if (setsockopt(s, SOL_SOCKET, SO_TIMESTAMPING,
			   &so_timestamping_flags,
			   sizeof(so_timestamping_flags)) < 0)
		{
			perror("setsockopt SO_TIMESTAMPING");
			fprintf(stderr, "Falling back to SW timestamps.\n");

			const int timestamp_on = 1;
			if (setsockopt(s, SOL_SOCKET, SO_TIMESTAMPNS,
					&timestamp_on, sizeof(timestamp_on)) < 0)
				error(1, errno, "setsockopt SO_TIMESTAMPNS");
		}

		const int dropmonitor_on = 1;
		if (setsockopt(s, SOL_SOCKET, SO_RXQ_OVFL,
			       &dropmonitor_on, sizeof(dropmonitor_on)) < 0)
			error(1, errno, "setsockopt SO_RXQ_OVFL not supported by your Linux Kernel");

		pfd[i].fd = s;
		if (i == 0)
			pfd[i].events = POLLIN | POLLERR | ((opt.period_us == 0 && !opt.oneattime) ? POLLOUT : 0);
		else
			pfd[i].events = POLLIN;
	}

	if (determine_can_timing_offset(pfd, num_interfaces) < 0) {
		perror("Failed to determine CAN timing offset.");
	}

	set_sched_policy_and_prio(SCHED_FIFO, 40);

#define SEND() send_and_check(pfd[0].fd)

	if (opt.oneattime)
		SEND();

	get_tstamp(&stats.tic);
	ret = 0;
	while (!finish_flag &&
	       (opt.count == 0 || count < opt.count || msg_in_progress != 0)) {

		get_next_timeout(&timeout, ret == 0);
		//printf("timeout %ld.%06ld\n", timeout.tv_sec, timeout.tv_nsec/1000);
		//printf("ppoll"); fflush(stdout);
		ret = ppoll(pfd, num_interfaces, &timeout, NULL);
		//printf("=%d\n", ret);
		switch (ret) {
		case -1: // Error
			if (!INTERRUPTED_SYSCALL(errno))
				error(1, errno, "ppoll");
			break;
		case 0: // Timeout
			if (opt.period_us) {
				if (opt.count == 0 || count < opt.count) {
					SEND();
				}
			} else {
				/* Lost message - send a new one */
				stats.timeouts++;
				consecutive_timeouts++;
				if (consecutive_timeouts < 10)
					SEND();
				else /* Something is really broken */
					finish_flag = 1;
			}
			break;
		default: // Event
			if (pfd[0].revents & (POLLIN|POLLERR)) {
				process_tx(pfd[0].fd);
			}
			if (pfd[0].revents & POLLOUT) {
				if (opt.count == 0 || count < opt.count)
					SEND();
			}
			pfd[0].revents = 0;

			if (num_interfaces == 3 && pfd[1].revents & POLLIN) {
				process_on_wire_rx(pfd[1].fd);
				pfd[1].revents = 0;
			}
			if (num_interfaces == 3 && pfd[1].revents & ~POLLIN)
				error(1, 0, "Unexpected pfd[1].revents: 0x%04x\n", pfd[1].revents);

			i = (num_interfaces == 2) ? 1 : 2;
			if (pfd[i].revents & POLLIN) {
				consecutive_timeouts = 0;
				if (process_final_rx(pfd[i].fd) == 0) {
					msg_in_progress--;
					if ((opt.count == 0 || count < opt.count) && opt.oneattime) {
						SEND();
					}
				}
				pfd[i].revents = 0;
			}
			if (pfd[i].revents & ~POLLIN)
				error(1, 0, "Unexpected pfd[%d].revents: 0x%04x\n", i, pfd[i].revents);
		}
	}

	get_tstamp(&stats.tac);

	for (i=0; i<num_interfaces; i++)
		close(pfd[i].fd);

	return NULL;
}

struct poptOption optionsTable[] = {
	{ "device", 'd', POPT_ARG_ARGV, &opt.interface, 'd', "Interface to use. Must be given two times (tx, rx) or three times (tx, rx1, rx2)", "interface" },
	{ "count",  'c', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,  &opt.count,	0,   "The count of messages to send, zero corresponds to infinity", "num"},
	{ "id",     'i', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,  &opt.id, 	0,   "CAN ID of sent messages", "id"},
	{ "period", 'p', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,  &opt.period_us, 0, "Period for sending messages or zero (default) to send as fast as possible", "us"},
	{ "timeout",'t', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,  &opt.timeout_ms,0, "Timeout when period is zero", "ms"},
	{ "oneattime",'o', POPT_ARG_NONE,			  &opt.oneattime,0,  "Send the next message only when the previous was finally received"},
	{ "verbose",'v', POPT_ARG_NONE,			  	  NULL, 'v',   	     "Send the next message only when the previous was finally received"},
	{ "name",   'n', POPT_ARG_STRING,			  &opt.name, 0,	     "Prefix of the generated files"},
	{ "length", 'l', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT,  &opt.length, 0,    "The length of generated messages", "bytes"},
	{ "userhist", 'u', POPT_ARG_NONE,  			  &opt.userhist, 0,  "Generate histogram from userspace timestamps"},
	{ "quiet",  'q', POPT_ARG_NONE,  			  &opt.quiet, 0,     "Do not print progress and statistics"},
	{ "fd", 'f', POPT_ARG_NONE, &opt.fd, 0, "Send CAN FD frames" },
	{ "bitrate", 'b', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT, &opt.bitrate, 0, "CAN bit rate (in bites per second)" },
	{ "dbitrate", '\0', POPT_ARG_INT|POPT_ARGFLAG_SHOW_DEFAULT, &opt.dbitrate, 0, "CAN FD data bit rate (in bites per second)" },
	{ "one-bus-measurement", '\0', POPT_ARG_NONE, &opt.one_bus_meas, 0, "Measurement on a single CAN bus. Expects CAN frames with id-1."},
	{ "disable-can-filters", '\0', POPT_ARG_NONE, &opt.disable_can_filters, 0, "Disable SocketCAN filters for single bus measurement. Default false."},
	POPT_AUTOHELP
	{ NULL, 0, 0, NULL, 0 }
};

int parse_options(int argc, const char *argv[])
{
	int c;
	poptContext optCon;   /* context for parsing command-line options */
	void *local = talloc_new (NULL);

	optCon = poptGetContext(NULL, argc, argv, optionsTable, 0);
	//poptSetOtherOptionHelp(optCon, "[OPTIONS]* <port>");

	/* Now do options processing */
	while ((c = poptGetNextOpt(optCon)) >= 0) {
		switch (c) {
		case 'd':
			num_interfaces++;
			break;
		}
	}
	if (c < -1)
		error(1, 0, "%s: %s\n",
		      poptBadOption(optCon, POPT_BADOPTION_NOALIAS),
		      poptStrerror(c));

	if (num_interfaces < 1 || num_interfaces > 3)
		error(1, 0, "-d option must only be given one, two or three times");

	if (opt.oneattime && opt.period_us)
		error(1, 0, "oneattime and period cannot be specified at the same time");

	if (opt.name) {
		char *f = talloc_asprintf(local, "%s-msgs.txt", opt.name);
		opt.f_msgs = fopen(f, "w");
		if (!opt.f_msgs)
			error(1, errno, "fopen: %s", f);
	}

	if (opt.name) {
		char *f = talloc_asprintf(local, "%s-hist-raw.txt", opt.name);
		opt.f_hist = fopen(f, "w");
		if (!opt.f_hist)
			error(1, errno, "fopen: %s", f);
	}

	if (opt.name) {
		char *f = talloc_asprintf(local, "%s-hist.txt", opt.name);
		opt.f_hist_gw = fopen(f, "w");
		if (!opt.f_hist_gw)
			error(1, errno, "fopen: %s", f);
	}

	if (opt.name) {
		char *f = talloc_asprintf(local, "%s-stat.txt", opt.name);
		opt.f_stat = fopen(f, "w");
		if (!opt.f_stat)
			error(1, errno, "fopen: %s", f);
	}

	if (opt.length > 8 && !opt.fd) {
		error(1, 0, "can't send frames longer than 8 bytes without CAN FD enabled");
	}

	if (opt.length > 64) {
		error(1, 0, "can't send frames longer than 64 bytes (even with CAN FD enabled)");
	}

	poptFreeContext(optCon);
	talloc_free(local);
	return 0;
}

void print_progress()
{
	if (! opt.quiet) {
		if (num_interfaces > 1)
			printf("\rSent %5d, in progress %5d", count, msg_in_progress);
		else
			printf("\rSent %5d", count);
		fflush(stdout);
	}
}

int main(int argc, const char *argv[])
{
	pthread_t thread;
	int ret, i;

	parse_options(argc, argv);

	mlockall(MCL_CURRENT | MCL_FUTURE);

	signal(SIGINT, term_handler);
	signal(SIGTERM, term_handler);

	for (i=0; i<MAX_INFOS; i++)
		msg_infos[i].id = -1;

	histogram_init(&histogram, 5000000, 1);
	histogram_init(&histogram_gw, 5000000, 1);

	ret = pipe(completion_pipe);
	if (ret == -1)
		error(1, errno, "pipe");
	ret = fcntl(completion_pipe[1], F_SETFL, O_NONBLOCK);
	if (ret == -1)
		error(1, errno, "pipe fcntl");

	init_ftrace();
	if (getenv("LATESTER_CONTROL_HACKBENCH")) {
		char cmd[1000];
		sprintf(cmd, "ssh -x -a -S $HOME/.ssh/cangw-connection root@192.168.2.3 'kill -CONT -%s'",
			getenv("LATESTER_CONTROL_HACKBENCH"));
		printf("Running: %s\n", cmd);
		system(cmd);
	}

	pthread_create(&thread, 0, measure_thread, NULL);

	struct timespec next, now, diff, allsent = {0,0};
	clock_gettime(CLOCK_MONOTONIC, &next);
	int completed = 0;
	while (!finish_flag && (opt.count == 0 || completed < opt.count)) {
		struct pollfd pfd[1];
		pfd[0].fd = completion_pipe[0];
		pfd[0].events = POLLIN;
		ret = poll(pfd, 1, 100);
		if (ret == -1 && !INTERRUPTED_SYSCALL(errno))
			error(1, errno, "poll main");
		if (ret > 0 && (pfd[0].revents & POLLIN)) {
			struct msg_info *mi;
			int ret;
			ret = read(completion_pipe[0], &mi, sizeof(mi));
			if (ret < sizeof(mi))
				error(1, errno, "read completion returned %d", ret);

			msg_info_process(opt.f_msgs, mi);
			msg_info_free(mi);
			completed++;
		}

		clock_gettime(CLOCK_MONOTONIC, &now);
		if (timespec_subtract(&diff, &next, &now)) {
			print_progress();
			next.tv_nsec += 100000000;
			while (next.tv_nsec >= 1000000000) {
				next.tv_nsec -= 1000000000;
				next.tv_sec++;
			}
		}
		if (opt.count != 0 && count >= opt.count) {
			if (allsent.tv_sec == 0)
				allsent = now;
			timespec_subtract(&diff, &now, &allsent);
			if (diff.tv_sec * NSEC_PER_SEC + diff.tv_nsec >= 1 * NSEC_PER_SEC) {	// diff is greater than 1 sec
				finish_flag = 1;
			}
		}
	}
	print_progress();
	if (!opt.quiet)
		printf("\n");

	stats.lost = msg_in_progress;

	pthread_join(thread, NULL);

	if (getenv("LATESTER_CONTROL_HACKBENCH")) {
		char cmd[1000];
		sprintf(cmd, "ssh -x -a -S $HOME/.ssh/cangw-connection root@192.168.2.3 'kill -STOP -%s'",
			getenv("LATESTER_CONTROL_HACKBENCH"));
		printf("Running: %s\n", cmd);
		system(cmd);
	}

	close(completion_pipe[0]);
	close(completion_pipe[1]);

	histogram_fprint(&histogram, opt.f_hist);
	histogram_fprint(&histogram_gw, opt.f_hist_gw);
	if (opt.f_hist)
		fclose(opt.f_hist);
	if (opt.f_hist_gw)
		fclose(opt.f_hist_gw);
	if (opt.f_msgs)
		fclose(opt.f_msgs);

	if (opt.f_stat) {
		struct histogram_stats hs;
		fprintf(opt.f_stat, "cmdline='");
		for (i=0; i<argc; i++)
			fprintf(opt.f_stat, "%s%s", argv[i], i < argc-1 ? " " : "");
		fprintf(opt.f_stat, "'\n");

		timespec_subtract(&diff, &stats.tac, &stats.tic);
		fprintf(opt.f_stat, "duration=%s # seconds\n", tstamp_str(NULL, &diff));

		fprintf(opt.f_stat, "sent=%d\n", count);
		fprintf(opt.f_stat, "overrun=%d\n", stats.overrun);
		if (stats.overrun && !opt.quiet)
			printf("overrun=%d\n", stats.overrun);
		fprintf(opt.f_stat, "enobufs=%d\n", stats.enobufs);
		if (stats.enobufs && !opt.quiet)
			printf("enobufs=%d\n", stats.enobufs);
		fprintf(opt.f_stat, "lost=%d\n", stats.lost);
		if (stats.lost && !opt.quiet)
			printf("lost=%d\n", stats.lost);
		fprintf(opt.f_stat, "timeouts=%d\n", stats.timeouts);
		if (stats.timeouts && !opt.quiet)
			printf("timeouts=%d\n", stats.timeouts);
		fprintf(opt.f_stat, "invalid_frame=%d\n", stats.timeouts);
		if (stats.timeouts && !opt.quiet)
			printf("invalid_frame=%d\n", stats.timeouts);

		if (num_interfaces == 3) {
			fprintf(opt.f_stat, "ts_diff_sec=%ld\n", can_tsdiff.tv_sec);
			fprintf(opt.f_stat, "ts_diff_nsec=%ld\n", can_tsdiff.tv_nsec);
		}

		histogram_stats(&histogram_gw, &hs);
		double avg = hs.count ? (double)hs.sum/hs.count : 0;
		fprintf(opt.f_stat, "avg=%g\n", avg);
		fprintf(opt.f_stat, "out_of_range_below=%d\n", hs.below);
		fprintf(opt.f_stat, "out_of_range_above=%d\n", hs.above);
		for (i = 0; i <= 20; i++)
			fprintf(opt.f_stat, "percentile%d=%d\n", i*5, hs.percentile[i*5]);

		fclose(opt.f_stat);
	}

	return 0;
}
