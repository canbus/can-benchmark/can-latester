#include <stdio.h>
#include "canframelen.h"

int main(int argc, char *argv[])
{
	struct canfd_frame cf = { .can_id = 0x123, .len = 2, .data = { 0x00, 0xff } };
	printf("Length: %u\n", can_frame_length(&cf, CFL_EXACT, CAN_MTU));
	return 0;
}
