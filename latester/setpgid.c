/* SPDX-License-Identifier: GPL-2.0-or-later */

#define _XOPEN_SOURCE 500
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <command>\n", argv[0]);
		return 1;
	}
	pid_t pid;
	pid = atol(argv[1]);
	if (setpgid(0, 0) != 0) {
		perror("setpgid");
		return 1;
	}

	char **newargv = malloc(sizeof(void*)*argc);
	unsigned i;
	for (i=1; i<argc; i++)
		newargv[i-1] = argv[i];
	newargv[argc-1] = NULL;
	kill(0, SIGSTOP);
	execvp(argv[1], newargv);
	perror("execvp");   /* execve() only returns on error */
	exit(EXIT_FAILURE);
}
