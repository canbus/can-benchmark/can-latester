/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * CAN user space gateway for performance benchmarks
 * Copyright (C) 2013, 2014 Michal Sojka, DCE, FEE, CTU Prague
 * Copyright (C) 2022 Matej Vasilevski, DCE, FEE, CTU Prague
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
/*
 * The project is continuation to DCE, FEE, CTU Prague OCERA OrtCAN
 * project (origin 2003) CANping and other drivers and tools
 * which has been started and is maintained by Pavel Pisa at CTU FEE.
 * See CTU FEE CAN bus projects guidepost for components, continuous
 * integration, live results etc. https://canbus.pages.fel.cvut.cz/
 */

#define _GNU_SOURCE
#include <sys/socket.h>
#include <linux/can.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <poll.h>
#include <sys/mman.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <linux/can/raw.h>

#ifndef SO_BUSY_POLL
#define SO_BUSY_POLL 46
#endif

#define FRAME_SIZE 128
#define BLOCK_SIZE 4096
#define BLOCK_NR 2
#define FRAME_NR (BLOCK_NR*(BLOCK_SIZE/FRAME_SIZE))

#define MMSG_MAX 64

#define STRINGIFY(val) #val
#define TOSTRING(val) STRINGIFY(val)
#define CHECK(cmd) ({ int ret = (cmd); if (ret == -1) { perror(#cmd " line " TOSTRING(__LINE__)); exit(1); }; ret; })
#define CHECKPTR(cmd) ({ void *ptr = (cmd); if (ptr == (void*)-1) { perror(#cmd " line " TOSTRING(__LINE__)); exit(1); }; ptr; })

char *devin = "can0";
char *devout = "can1";
enum { IN_READ, IN_RECVMMSG, IN_MMAP, IN_MMAPBUSY } in_method = IN_READ;
enum { WRITE, OUT_MMAP, OUT_SENDMMSG } out_method = WRITE;
bool quiet = false;
int busy_poll_us = 0;
bool nonblocking = false;
unsigned int can_frame_mtu = CAN_MTU;
int enable_canfd = 0;

enum in2out {
	STORE_ONLY,
	SEND,
	NOP,
};


#define VERBOSE(format, ...) do { if (!quiet) fprintf(stderr, format, ##__VA_ARGS__); } while (0)

struct in_ctx {
	int s;
	enum in2out (*in_fn)(struct in_ctx *ctx, struct canfd_frame *cf);
	/* mmap */
	void *ptr;
	int hdrlen;
	int current;
	/* mmsg */
	int received;
	struct mmsghdr msgs[MMSG_MAX];
	struct iovec iovecs[MMSG_MAX];
	char bufs[MMSG_MAX][sizeof(struct canfd_frame)];	// FIXME this is incorrect
		// allocating the bufs array depending on can_frame_mtu would be better
		// but I'll use the ugw only with read/write syscalls,
		// so why bother now (YAGNI)
};

struct out_ctx {
	int s;
	enum in2out from_in;
	int (*out_fn)(struct out_ctx *ctx, struct canfd_frame *cf);
	/* mmap */
	void *ptr;
	int hdrlen;
	int current;
	/* mmsg */
	int stored;
	struct mmsghdr msgs[MMSG_MAX];
	struct iovec iovecs[MMSG_MAX];
	char bufs[MMSG_MAX][sizeof(struct canfd_frame)];	// FIXME dtto
};

struct stats {
	int store;
	int send;
} stats;

void sigint(int v)
{
	printf("store:%d\nsend:%d\ntotal:%d\n",
	       stats.store, stats.send, stats.store + stats.send);
	exit(0);
}

enum in2out in_read(struct in_ctx *ctx, struct canfd_frame *cf)
{
	int ret = read(ctx->s, cf, can_frame_mtu);
	if (nonblocking && ret == -1 && errno == EAGAIN)
		return NOP;
	if (ret != can_frame_mtu) {
		perror("read");
		exit(1);
	}
	return STORE_ONLY;
}

void init_read(struct in_ctx *ctx)
{
	int s;
	struct sockaddr_can addr;
	struct ifreq ifr;

	s = CHECK(socket(PF_CAN, SOCK_RAW, CAN_RAW));

	int rcvbuf = 25000;	/* Limit rcvbuf to not have so big queueing latencies */
	CHECK(setsockopt(s, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf)));
	if (enable_canfd == 1)
		CHECK(setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &enable_canfd, sizeof(enable_canfd)));

	if (nonblocking) {
		int flags = CHECK(fcntl(s, F_GETFL, 0));
		CHECK(fcntl(s, F_SETFL, flags | O_NONBLOCK));
	}

	if (busy_poll_us) {
		CHECK(setsockopt(s, SOL_SOCKET, SO_BUSY_POLL,
				 &busy_poll_us, sizeof(busy_poll_us)));
	}

	strncpy(ifr.ifr_name, devin, sizeof(ifr.ifr_name));
	if (-1 == ioctl(s, SIOCGIFINDEX, &ifr)) {
		perror(devin);
		exit(1);
	}

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	CHECK(bind(s, (struct sockaddr *)&addr, sizeof(addr)));

	ctx->s = s;
	ctx->in_fn = in_read;
}

enum in2out in_recvmmsg(struct in_ctx *ctx, struct canfd_frame *cf)
{
	int ret;

	if (ctx->current >= ctx->received) {
		ret = CHECK(recvmmsg(ctx->s, ctx->msgs, MMSG_MAX, MSG_WAITFORONE, NULL));
		if (nonblocking && ret == -1 && errno == EAGAIN)
			return NOP;
		ctx->received = ret;
		ctx->current = 0;
	}

	memcpy(cf, ctx->bufs[ctx->current], can_frame_mtu);
	ctx->current++;

	return (ctx->current < ctx->received) ? STORE_ONLY : SEND;
}

void init_recvmmg(struct in_ctx *ctx)
{
	int i;

	init_read(ctx);
	ctx->in_fn = in_recvmmsg;
	ctx->current = 0;
	memset(ctx->msgs, 0, sizeof(ctx->msgs));
	memset(ctx->iovecs, 0, sizeof(ctx->iovecs));
	for (i = 0; i < MMSG_MAX; i++) {
		ctx->iovecs[i].iov_base         = ctx->bufs[i];
		ctx->iovecs[i].iov_len          = can_frame_mtu;
		ctx->msgs[i].msg_hdr.msg_iov    = &ctx->iovecs[i];
		ctx->msgs[i].msg_hdr.msg_iovlen = 1;
	}
}

enum in2out in_packet_rx(struct in_ctx *ctx, struct canfd_frame *cf)
{
	volatile struct tpacket2_hdr *hdr = ctx->ptr + ctx->current*FRAME_SIZE;
	int ret = -1;

	while (hdr->tp_status == TP_STATUS_KERNEL) {
		if (in_method != IN_MMAPBUSY) {
			struct pollfd pfd = {.fd = ctx->s, .revents = 0,
					     .events = POLLIN|POLLRDNORM|POLLERR };
			ret = CHECK(poll(&pfd, 1, -1));
		}
	}
	//struct sockaddr_ll *addr = (void*)hdr + TPACKET_ALIGN(ctx->hdrlen);
	(void)ret;
	struct canfd_frame *cf_mmap = (void*)hdr + hdr->tp_mac;
	*cf = *cf_mmap;
	hdr->tp_status = 0;
	ctx->current = (ctx->current + 1) % FRAME_NR;
	hdr = ctx->ptr + ctx->current*FRAME_SIZE;
	return (hdr->tp_status == TP_STATUS_KERNEL) ? SEND : STORE_ONLY;
}


void init_packet_rx(struct in_ctx *ctx)
{
	int s;
	struct sockaddr_ll my_addr;
	struct ifreq ifr;

	s = CHECK(socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL)));

	if (busy_poll_us) {
		CHECK(setsockopt(s, SOL_SOCKET, SO_BUSY_POLL,
				 &busy_poll_us, sizeof(busy_poll_us)));
	}

	int val = TPACKET_V2;
	CHECK(setsockopt(s, SOL_PACKET, PACKET_VERSION, &val, sizeof(val)));
	socklen_t len = sizeof(ctx->hdrlen);
	CHECK(getsockopt(s, SOL_PACKET, PACKET_HDRLEN, &ctx->hdrlen, &len));

	strncpy (ifr.ifr_name, devin, sizeof(ifr.ifr_name));
	CHECK(ioctl(s, SIOCGIFINDEX, &ifr));

	my_addr.sll_family = AF_PACKET;
	my_addr.sll_protocol = htons(ETH_P_ALL);
	my_addr.sll_ifindex =  ifr.ifr_ifindex;

	CHECK(bind(s, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_ll)));

	struct tpacket_req req = {
		.tp_block_size = BLOCK_SIZE,
		.tp_frame_size = FRAME_SIZE,
		.tp_block_nr   = BLOCK_NR,
		.tp_frame_nr   = FRAME_NR,
	};
	CHECK(setsockopt(s, SOL_PACKET, PACKET_RX_RING, (char *)&req, sizeof(req)));

	ctx->ptr = (char*)CHECKPTR(mmap(0, BLOCK_SIZE*BLOCK_NR, PROT_READ|PROT_WRITE, MAP_SHARED, s, 0));

	ctx->s = s;
	ctx->in_fn = in_packet_rx;
	ctx->current = 0;
}

int out_write(struct out_ctx *ctx, struct canfd_frame *cf)
{
	if (ctx->from_in == NOP)
		return 0;

	int ret = write(ctx->s, cf, can_frame_mtu);
	if (ret != can_frame_mtu) {
		perror("write");
		exit(1);
	}
	return 0;
}

void init_write(struct out_ctx *ctx)
{
	int s;
	struct sockaddr_can addr;
	struct ifreq ifr;

	s = CHECK(socket(PF_CAN, SOCK_RAW, CAN_RAW));
	if (enable_canfd == 1)
		CHECK(setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &enable_canfd, sizeof(enable_canfd)));

	strncpy(ifr.ifr_name, devout, sizeof(ifr.ifr_name));
	if (-1 == ioctl(s, SIOCGIFINDEX, &ifr)) {
		perror(devout);
		exit(1);
	}

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	CHECK(bind(s, (struct sockaddr *)&addr, sizeof(addr)));

	ctx->s = s;
	ctx->out_fn = out_write;
}

int out_sendmmsg(struct out_ctx *ctx, struct canfd_frame *cf)
{
	if (ctx->from_in == NOP)
		return 0;

	memcpy(ctx->bufs[ctx->current++], cf, can_frame_mtu);

	if (ctx->from_in == SEND ||
	    ctx->current >= MMSG_MAX) {
		int ret, i;
		for (i = 0; i < ctx->current; i += ret)
			ret = CHECK(sendmmsg(ctx->s, &ctx->msgs[i], ctx->current - i, 0));
		ctx->current = 0;
	}
	return 0;
}

void init_sendmmsg(struct out_ctx *ctx)
{
	int i;

	init_write(ctx);
	ctx->out_fn = out_sendmmsg;
	ctx->current = 0;
	ctx->stored = 0;

	memset(ctx->msgs, 0, sizeof(ctx->msgs));
	memset(ctx->iovecs, 0, sizeof(ctx->iovecs));
	for (i = 0; i < MMSG_MAX; i++) {
		ctx->iovecs[i].iov_base         = &ctx->bufs[i];
		ctx->iovecs[i].iov_len          = can_frame_mtu;
		ctx->msgs[i].msg_hdr.msg_iov    = &ctx->iovecs[i];
		ctx->msgs[i].msg_hdr.msg_iovlen = 1;
	}
}

int out_packet_tx(struct out_ctx *ctx, struct canfd_frame *cf)
{
	volatile struct tpacket2_hdr *hdr = ctx->ptr + ctx->current*FRAME_SIZE;

	if (ctx->from_in == NOP) {
		CHECK(send(ctx->s, NULL, 0, 0));
		return 0;
	}

	while (hdr->tp_status != TP_STATUS_AVAILABLE) {
		CHECK(send(ctx->s, NULL, 0, 0));
	}

	//struct sockaddr_ll *addr = (void*)hdr + TPACKET_HDRLEN - sizeof(struct sockaddr_ll);
	struct canfd_frame *cf_mmap = (void*)hdr + TPACKET_HDRLEN  - sizeof(struct sockaddr_ll);
	*cf_mmap = *cf;
	hdr->tp_len = can_frame_mtu;
	hdr->tp_status = TP_STATUS_SEND_REQUEST;
	ctx->current = (ctx->current + 1) % FRAME_NR;

	if (ctx->from_in == SEND){
		CHECK(send(ctx->s, NULL, 0, 0));
		return 0;
	}
	return 0;
}


void init_packet_tx(struct out_ctx *ctx)
{
	int s;
	struct sockaddr_ll my_addr;
	struct ifreq ifr;

	s = CHECK(socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL)));

	int val = TPACKET_V2;
	CHECK(setsockopt(s, SOL_PACKET, PACKET_VERSION, &val, sizeof(val)));
	socklen_t len = sizeof(ctx->hdrlen);
	CHECK(getsockopt(s, SOL_PACKET, PACKET_HDRLEN, &ctx->hdrlen, &len));

	strncpy (ifr.ifr_name, devout, sizeof(ifr.ifr_name));
	CHECK(ioctl(s, SIOCGIFINDEX, &ifr));

	my_addr.sll_family = AF_PACKET;
	my_addr.sll_protocol = htons(ETH_P_CAN);
	my_addr.sll_ifindex =  ifr.ifr_ifindex;

	CHECK(bind(s, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_ll)));

	struct tpacket_req req = {
		.tp_block_size = BLOCK_SIZE,
		.tp_frame_size = FRAME_SIZE,
		.tp_block_nr   = BLOCK_NR,
		.tp_frame_nr   = FRAME_NR,
	};
	CHECK(setsockopt(s, SOL_PACKET, PACKET_TX_RING, (char *)&req, sizeof(req)));

	ctx->ptr = (char*)CHECKPTR(mmap(0, BLOCK_SIZE*BLOCK_NR, PROT_READ|PROT_WRITE, MAP_SHARED, s, 0));

	ctx->s = s;
	ctx->out_fn = out_packet_tx;
	ctx->current = 0;
}

void init_in(struct in_ctx *in)
{
	memset(in, 0, sizeof(*in));
	switch (in_method) {
	case IN_READ:
		init_read(in);
		break;
	case IN_RECVMMSG:
		init_recvmmg(in);
		break;
	case IN_MMAP:
	case IN_MMAPBUSY:
		init_packet_rx(in);
		break;
	default:
		fprintf(stderr, "Unknown \"in method\" %d\n", in_method);
		exit(1);
	}
}

int in(struct in_ctx *ctx, struct canfd_frame *cf)
{
	return ctx->in_fn(ctx, cf);
}

void init_out(struct out_ctx *out)
{
	memset(out, 0, sizeof(*out));
	switch (out_method) {
	case WRITE: init_write(out); break;
	case OUT_SENDMMSG: init_sendmmsg(out); break;
	case OUT_MMAP: init_packet_tx(out); break;
	default:
		fprintf(stderr, "Unknown \"out method\" %d\n", out_method);
		exit(1);
	}
}

int out(struct out_ctx *ctx, struct canfd_frame *cf)
{
	return ctx->out_fn(ctx, cf);
}

void gw()
{
	struct in_ctx    ic;
	struct out_ctx   oc;
	struct canfd_frame cf;

	init_in(&ic);
	init_out(&oc);

	VERBOSE("UGW started\n");

	while (1) {
		oc.from_in = in(&ic, &cf);
		switch (oc.from_in) {
		case SEND:       stats.send++;  break;
		case STORE_ONLY: stats.store++; break;
		case NOP:	 		break;
		}
		out(&oc, &cf);
	}
}


int main(int argc, char *argv[])
{
	int opt;

	while ((opt = getopt(argc, argv, "b:nqfr:t:")) != -1) {
		switch (opt) {
		case 'b':
			busy_poll_us = atoi(optarg);
			break;
		case 'n':
			nonblocking = true;
			break;
		case 'q':
			quiet = true;
			break;
		case 'f':
			can_frame_mtu = CANFD_MTU;
			enable_canfd = 1;
			break;
		case 'r':
			if (strcmp(optarg, "read") == 0)
				in_method = IN_READ;
			else if (strcmp(optarg, "mmap") == 0)
				in_method = IN_MMAP;
			else if (strcmp(optarg, "mmapbusy") == 0)
				in_method = IN_MMAPBUSY;
			else if (strcmp(optarg, "mmsg") == 0)
				in_method = IN_RECVMMSG;
			else {
				fprintf(stderr, "Unsuported RX method: %s\n", optarg);
				exit(1);
			}
			break;
		case 't':
			if (strcmp(optarg, "write") == 0)
				out_method = WRITE;
			else if (strcmp(optarg, "mmap") == 0)
				out_method = OUT_MMAP;
			else if (strcmp(optarg, "mmsg") == 0)
				out_method = OUT_SENDMMSG;
			else {
				fprintf(stderr, "Unsuported TX method: %s\n", optarg);
				exit(1);
			}
			break;
		default: /* '?' */
			fprintf(stderr, "Usage: %s [in_interface out_interface]\n",
				argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	if (optind < argc)
		devin  = argv[optind];
	if (optind+1 < argc)
		devout = argv[optind+1];

	signal(SIGINT, sigint);
	gw();

	return 0;
}
